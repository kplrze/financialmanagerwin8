﻿using FinancialManagerWin8.Common;
using FinancialManagerWin8.ViewModel;
//using FinancialManagerWin8.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace FinancialManagerWin8.View.Categories
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class CategoryEditPage : Page
    {
        #region Properties & Fields
        private CategoryEditViewModel viewModel = null;

        #region NavigationHelper
        private NavigationHelper navigationHelper;
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }
        #endregion

        #endregion

        #region CategoryEditPage()
        public CategoryEditPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;

            viewModel = new CategoryEditViewModel();
            this.DataContext = viewModel;
        } 
        #endregion

        #region navigationHelper_LoadState()
        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            await viewModel.InitAsync();

            int id = Convert.ToInt32(e.NavigationParameter);
            if(id > 0)
            {
                viewModel.RecordId = id;
                await viewModel.DataLoadAsync();
            }
        }
        #endregion

        #region navigationHelper_SaveState()
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {

        }
        #endregion

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
