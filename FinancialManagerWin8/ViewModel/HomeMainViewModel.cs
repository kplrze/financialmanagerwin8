﻿using FinancialManagerWin8.Common;
using FinancialManagerWin8.DataAccessLayer;
using FinancialManagerWin8.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace FinancialManagerWin8.ViewModel
{
    public class HomeMainViewModel : VMBase
    {
        #region Properties & Fields

        private IExpenseRepository expenseRepository;
        private IRepository<Category> categoryRepository;

        #region MonthFirst
        private string monthFirst = String.Empty;
        public string MonthFirst
        {
            get { return monthFirst; }
            set
            {
                if (monthFirst == value)
                {
                    return;
                }
                monthFirst = value;
                RaisePropertyChanged("MonthFirst");
            }
        }
        #endregion

        #region MonthSecond
        private string monthSecond = String.Empty;
        public string MonthSecond
        {
            get { return monthSecond; }
            set
            {
                if (monthSecond == value)
                {
                    return;
                }
                monthSecond = value;
                RaisePropertyChanged("MonthSecond");
            }
        }
        #endregion

        #region MonthThird
        private string monthThird = String.Empty;
        public string MonthThird
        {
            get { return monthThird; }
            set
            {
                if (monthThird == value)
                {
                    return;
                }
                monthThird = value;
                RaisePropertyChanged("MonthThird");
            }
        }
        #endregion

        #region ExpensesMonthFirstItems
        private ObservableCollection<ExpensesInCategoriesItem> expensesMonthFirstItems = null;
        public ObservableCollection<ExpensesInCategoriesItem> ExpensesMonthFirstItems
        {
            get { return expensesMonthFirstItems; }
            private set
            {
                expensesMonthFirstItems = value;
                RaisePropertyChanged("ExpensesMonthFirstItems");
            }
        }
        #endregion

        #region ExpensesMonthSecondItems
        private ObservableCollection<ExpensesInCategoriesItem> expensesMonthSecondItems = null;
        public ObservableCollection<ExpensesInCategoriesItem> ExpensesMonthSecondItems
        {
            get { return expensesMonthSecondItems; }
            private set
            {
                expensesMonthSecondItems = value;
                RaisePropertyChanged("ExpensesMonthSecondItems");
            }
        }
        #endregion

        #region ExpensesMonthThirdItems
        private ObservableCollection<ExpensesInCategoriesItem> expensesMonthThirdItems = null;
        public ObservableCollection<ExpensesInCategoriesItem> ExpensesMonthThirdItems
        {
            get { return expensesMonthThirdItems; }
            private set
            {
                expensesMonthThirdItems = value;
                RaisePropertyChanged("ExpensesMonthThirdItems");
            }
        }
        #endregion

        #region LastExpensesItems
        private ObservableCollection<Expense> lastExpensesItems = null;
        public ObservableCollection<Expense> LastExpensesItems
        {
            get { return lastExpensesItems; }
            private set
            {
                lastExpensesItems = value;
                RaisePropertyChanged("LastExpensesItems");
            }
        }
        #endregion 

        #endregion

        #region HomeMainViewModel()
        public HomeMainViewModel()
        {
            expenseRepository = new ExpenseRepository();
            categoryRepository = new CategoryRepository();
        } 
        #endregion

        #region Init()
        public async Task InitAsync()
        {
            try
            {
                string[] months = AppHelpers.GetMonths(false);

                int firstMonthValue = DateTime.Now.Month;
                int firstMonthYear = DateTime.Now.Year;

                int secondMonthValue = DateTime.Now.AddMonths(-1).Month;
                int secondMonthYear = DateTime.Now.AddMonths(-1).Year;

                int thirdMonthValue = DateTime.Now.AddMonths(-2).Month;
                int thirdMonthYear = DateTime.Now.AddMonths(-2).Year;

                MonthFirst = months[firstMonthValue - 1] + " - " + firstMonthYear;
                MonthSecond = months[secondMonthValue - 1] + " - " + secondMonthYear;
                MonthThird = months[thirdMonthValue - 1] + " - " + thirdMonthYear;
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
        }
        #endregion

        #region DataLoadAsync()
        public async Task DataLoadAsync()
        {
            try
            {
                List<Category> categories = await categoryRepository.GetListAsync();

                LastExpensesItems = new ObservableCollection<Expense>();

                ExpensesMonthFirstItems = new ObservableCollection<ExpensesInCategoriesItem>();
                
                int firstMonthYear = DateTime.Now.Year;
                int firstMonthValue = DateTime.Now.Month;

                List<Expense> firstMonthExpenses = await expenseRepository.GetListAsync(firstMonthYear, firstMonthValue);
                
                foreach(Expense fmExpense in firstMonthExpenses)
                {
                    LastExpensesItems.Add(fmExpense);
                }

                decimal firstMonthExpensesSUM = firstMonthExpenses.Sum(i => i.Amount);

                var groupedFMITems = (from fmItem in firstMonthExpenses
                                      where fmItem.CategoryId > 0
                                      group fmItem by fmItem.CategoryId into fmGroup
                                      select new { CategoryId = fmGroup.Key, Amount = fmGroup.Sum(i => i.Amount) }).OrderByDescending(a => a.Amount); ;

                foreach (var expense in groupedFMITems.Take(5))
                {
                    Category expCategory = categories.FirstOrDefault(i => i.Id == expense.CategoryId);
                    if (expCategory != null)
                    {
                        ExpensesInCategoriesItem expCategoryItem = new ExpensesInCategoriesItem();
                        expCategoryItem.CategoryName = expCategory.Name;
                        expCategoryItem.Amount = expense.Amount;
                        if (firstMonthExpensesSUM > 0)
                        {
                            expCategoryItem.PercentOfTotal = (expense.Amount / firstMonthExpensesSUM) * 100;
                        }

                        ExpensesMonthFirstItems.Add(expCategoryItem);
                    }
                }

                ExpensesMonthSecondItems = new ObservableCollection<ExpensesInCategoriesItem>();

                int secondMonthValue = DateTime.Now.AddMonths(-1).Month;
                int secondMonthYear = DateTime.Now.AddMonths(-1).Year;

                List<Expense> secondMonthExpenses = await expenseRepository.GetListAsync(secondMonthYear, secondMonthValue);

                foreach(Expense smExpense in secondMonthExpenses)
                {
                    LastExpensesItems.Add(smExpense);
                }

                decimal secondMonthExpensesSUM = secondMonthExpenses.Sum(i => i.Amount);

                var groupedSMItems = (from smItem in secondMonthExpenses
                                      where smItem.CategoryId > 0
                                      group smItem by smItem.CategoryId into smGroup
                                      select new { CategoryId = smGroup.Key, Amount = smGroup.Sum(i => i.Amount) }).OrderByDescending(a => a.Amount); ;

                foreach (var expense in groupedSMItems.Take(5))
                {
                    Category expCategory = categories.FirstOrDefault(i => i.Id == expense.CategoryId);
                    if (expCategory != null)
                    {
                        ExpensesInCategoriesItem expCategoryItem = new ExpensesInCategoriesItem();
                        expCategoryItem.CategoryName = expCategory.Name;
                        expCategoryItem.Amount = expense.Amount;
                        if (secondMonthExpensesSUM > 0)
                        {
                            expCategoryItem.PercentOfTotal = (expense.Amount / secondMonthExpensesSUM) * 100;
                        }

                        ExpensesMonthSecondItems.Add(expCategoryItem);
                    }
                }
                
                ExpensesMonthThirdItems = new ObservableCollection<ExpensesInCategoriesItem>();

                int thirdMonthValue = DateTime.Now.AddMonths(-2).Month;
                int thirdMonthYear = DateTime.Now.AddMonths(-2).Year;

                List<Expense> thirdMonthExpenses = await expenseRepository.GetListAsync(thirdMonthYear, thirdMonthValue);

                foreach(Expense tmExpense in thirdMonthExpenses)
                {
                    LastExpensesItems.Add(tmExpense);
                }

                decimal thirdMonthExpensesSUM = thirdMonthExpenses.Sum(i => i.Amount);

                var groupedTMItems = (from tmItem in thirdMonthExpenses
                                      where tmItem.CategoryId > 0
                                      group tmItem by tmItem.CategoryId into tmGroup
                                      select new { CategoryId = tmGroup.Key, Amount = tmGroup.Sum(i => i.Amount) }).OrderByDescending(a => a.Amount); ;

                foreach (var expense in groupedTMItems.Take(5))
                {
                    Category expCategory = categories.FirstOrDefault(i => i.Id == expense.CategoryId);
                    if (expCategory != null)
                    {
                        ExpensesInCategoriesItem expCategoryItem = new ExpensesInCategoriesItem();
                        expCategoryItem.CategoryName = expCategory.Name;
                        expCategoryItem.Amount = expense.Amount;
                        if (thirdMonthExpensesSUM > 0)
                        {
                            expCategoryItem.PercentOfTotal = (expense.Amount / thirdMonthExpensesSUM) * 100;
                        }

                        ExpensesMonthThirdItems.Add(expCategoryItem);
                    }
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        }
        #endregion
    }
}
