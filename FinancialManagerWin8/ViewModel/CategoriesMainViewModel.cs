﻿using FinancialManagerWin8.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using FinancialManagerWin8.DataAccessLayer;
using FinancialManagerWin8.Common;
using FinancialManagerWin8.View.Categories;
using FinancialManagerWin8.Model.Enums;

namespace FinancialManagerWin8.ViewModel
{
    public class CategoriesMainViewModel : VMBase, IEntitiesViewModel
    {
        #region Properties & fields
        private IRepository<Category> repository;
 
        private ObservableCollection<Category> entries = null;
        public ObservableCollection<Category> Entries
        {
            get { return entries; }
            private set { entries = value; }
        }
        #endregion

        #region CategoriesMainViewModel()
        public CategoriesMainViewModel()
        {
            repository = new CategoryRepository();
            entries = new ObservableCollection<Category>();
        } 
        #endregion

        #region DataLoadAsync()
        public async Task DataLoadAsync()
        {
            try
            {
                List<Category> categories = await repository.GetListAsync();

                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                foreach (Category category in categories)
                {
                    string partLang = Enum.GetName(typeof(TransactionType), category.Type);
                    category.TypeName = loader.GetString("TransactionType_" + partLang);

                    entries.Add(category);
                }
            }
            catch(Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region GoToEditEntityPage()
        public void GoToEditEntityPage(int id)
        {
            NavigateService.Instance.Navigate(typeof(CategoryEditPage), id);
        } 
        #endregion
    }
}
