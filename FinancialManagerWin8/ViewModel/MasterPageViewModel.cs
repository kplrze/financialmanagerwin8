﻿using FinancialManagerWin8.View.Categories;
using FinancialManagerWin8.Common;
using FinancialManagerWin8.View.Expenses;
using FinancialManagerWin8.View.Incomes;
using FinancialManagerWin8.Model;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Windows.UI.Xaml.Media.Imaging;
using FinancialManagerWin8.View;
using FinancialManagerWin8.View.Balance;
using FinancialManagerWin8.View.Statements;
using FinancialManagerWin8.View.Home;
using System.Threading.Tasks;
using FinancialManagerWin8.DataAccessLayer;
using System.Linq;
using FinancialManagerWin8.Model.Enums;
using FinancialManagerWin8.View.Dictionaries;

namespace FinancialManagerWin8.ViewModel
{
    public class MasterPageViewModel : VMBase
    {
        #region Properties & Fields
        private IExpenseRepository expenseRepository;
        private IIncomeRepository incomeRepository;
        private ICategoryRepository categoryRepository;

        private ObservableCollection<MainMenuItem> mainMenuItems = null;
        public ObservableCollection<MainMenuItem> MainMenuItems
        {
            get { return mainMenuItems; }
            private set
            {
                mainMenuItems = value;
                RaisePropertyChanged("MainMenuItems");
            }
        }

        public ICommand MainMenuSelectionChangedCmd { get; private set; }
        public ICommand GoToPageCmd { get; private set; }
        public ICommand GoToExpenseEditPageCmd { get; private set; }
        public ICommand GoToIncomeEditPageCmd { get; private set; }
        public ICommand GoToCategoryEditPageCmd { get; set; } 
        #endregion

        public MasterPageViewModel()
        {
            expenseRepository = new ExpenseRepository();
            incomeRepository = new IncomeRepository();
            categoryRepository = new CategoryRepository();

            mainMenuItems = new ObservableCollection<MainMenuItem>();

            this.GoToPageCmd = new RelayCommand<Type>(GoToPage);
            this.GoToExpenseEditPageCmd = new RelayCommand(GoToExpenseEditPage);
            this.GoToIncomeEditPageCmd = new RelayCommand(GoToIncomeEditPage);
            this.GoToCategoryEditPageCmd = new RelayCommand(GoToCategoryEditPage);
        }

        #region Commands methods
        private void GoToPage(Type page)
        {
            NavigateService.Instance.Navigate(page, typeof(MasterPage));
        }

        private void GoToExpenseEditPage()
        {
            NavigateService.Instance.Navigate(typeof(ExpenseEditPage));
        }

        private void GoToIncomeEditPage()
        {
            NavigateService.Instance.Navigate(typeof(IncomeEditPage));
        }

        private void GoToCategoryEditPage()
        {
            NavigateService.Instance.Navigate(typeof(CategoryEditPage));
        }
        #endregion

        #region InitPage()
        public async Task InitPageAsync()
        {
            try
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                mainMenuItems.Clear();

                MainMenuItem balanceMenuItem = new MainMenuItem();
                balanceMenuItem.Text = "Informacje podstawowe";
                balanceMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Balance3.png", UriKind.Absolute);
                balanceMenuItem.Visible = true;
                balanceMenuItem.Page = typeof(HomeMainPage);
                balanceMenuItem.MasterPage = true;

                MainMenuItems.Add(balanceMenuItem);

                MainMenuItem expenseMenuItem = new MainMenuItem();
                expenseMenuItem.Text = loader.GetString("MainMenu_Expenses");
                expenseMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Expenses2.png", UriKind.Absolute);
                expenseMenuItem.Visible = true;
                expenseMenuItem.Page = typeof(ExpensesMainPage);
                expenseMenuItem.MasterPage = true;

                mainMenuItems.Add(expenseMenuItem);

                MainMenuItem incomesMenuItem = new MainMenuItem();
                incomesMenuItem.Text = loader.GetString("MainMenu_Incomes");
                incomesMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Incomes2.png", UriKind.Absolute);
                incomesMenuItem.Page = typeof(IncomesMainPage);
                incomesMenuItem.MasterPage = true;

                mainMenuItems.Add(incomesMenuItem);

                MainMenuItem reportsMenuItem = new MainMenuItem();
                reportsMenuItem.Text = loader.GetString("MainMenu_Statements_Charts");
                reportsMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Reports2.png", UriKind.Absolute);
                reportsMenuItem.Visible = true;
                reportsMenuItem.Page = typeof(StatementsMainPage);
                reportsMenuItem.MasterPage = true;

                mainMenuItems.Add(reportsMenuItem);

                MainMenuItem dictionariesMenuItem = new MainMenuItem();
                dictionariesMenuItem.Text = loader.GetString("MainMenu_Dictionaries");
                dictionariesMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Dictionaries2.png", UriKind.Absolute);
                dictionariesMenuItem.Visible = true;
                dictionariesMenuItem.Page = typeof(DictionariesMainPage);
                dictionariesMenuItem.MasterPage = true;

                mainMenuItems.Add(dictionariesMenuItem);

                MainMenuItem categoriesMenuItem = new MainMenuItem();
                categoriesMenuItem.Text = loader.GetString("MainMenu_Categories");
                categoriesMenuItem.Icon = new Uri("ms-appx:///Themes/Light/Categories.png", UriKind.Absolute);
                categoriesMenuItem.Visible = true;
                categoriesMenuItem.Page = typeof(CategoriesMainPage);
                categoriesMenuItem.MasterPage = true;

                mainMenuItems.Add(categoriesMenuItem);

                NavigateService.Instance.Navigate(typeof(HomeMainPage), typeof(MasterPage));
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
        } 
        #endregion

        #region InitDatabase()
        public async Task InitDatabase()
        {
            try
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();


                List<Category> categories = await categoryRepository.GetListAsync();
                if(!categories.Any())
                {
                    //Kategorie przychodów

                    Category salaryCategory = new Category();
                    salaryCategory.Name = loader.GetString("category_Salary");
                    salaryCategory.Type = Convert.ToInt32(TransactionType.Income);

                    await categoryRepository.AddAsync(salaryCategory);

                    Category projectsOrdersCategory = new Category();
                    projectsOrdersCategory.Name = loader.GetString("category_ProjectsOrders");
                    projectsOrdersCategory.Type = Convert.ToInt32(TransactionType.Income);

                    await categoryRepository.AddAsync(projectsOrdersCategory);

                    Category otherIncomes = new Category();
                    otherIncomes.Name = loader.GetString("category_OtherIncomes");
                    otherIncomes.Type = Convert.ToInt32(TransactionType.Income);

                    await categoryRepository.AddAsync(otherIncomes);

                    //Kategorie wydatków

                    Category billsCategory = new Category();
                    billsCategory.Name = loader.GetString("category_Bills");
                    billsCategory.Type = Convert.ToInt32(TransactionType.Expense);

                    await categoryRepository.AddAsync(billsCategory);

                    Category phoneCategory = new Category();
                    phoneCategory.Name = loader.GetString("category_Phone");
                    phoneCategory.Type = Convert.ToInt32(TransactionType.Expense);

                    await categoryRepository.AddAsync(phoneCategory);

                    Category foodCategory = new Category();
                    foodCategory.Name = loader.GetString("category_Food");
                    foodCategory.Type = Convert.ToInt32(TransactionType.Expense);

                    await categoryRepository.AddAsync(foodCategory);

                    Category clothingCategory = new Category();
                    clothingCategory.Name = loader.GetString("category_Clothing");
                    clothingCategory.Type = Convert.ToInt32(TransactionType.Expense);

                    await categoryRepository.AddAsync(clothingCategory);
                    
                    Category otherExpenses = new Category();
                    otherExpenses.Name = loader.GetString("category_OtherExpenses");
                    otherExpenses.Type = Convert.ToInt32(TransactionType.Expense);

                    await categoryRepository.AddAsync(otherExpenses);
                }
            }
            catch(Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
        }
        #endregion
    }
}
