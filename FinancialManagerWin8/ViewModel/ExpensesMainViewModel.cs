﻿using FinancialManagerWin8.Common;
using FinancialManagerWin8.DataAccessLayer;
using FinancialManagerWin8.Model;
using FinancialManagerWin8.Model.Enums;
using FinancialManagerWin8.View.Expenses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace FinancialManagerWin8.ViewModel
{
    public class ExpensesMainViewModel : VMBase, IEntitiesViewModel
    {
        #region Properties & fields
        private IRepository<Expense> expenseRepository;
        private ICategoryRepository categoryRepository;

        private ObservableCollection<Expense> entries = null;
        public ObservableCollection<Expense> Entries
        {
            get { return entries; }
            private set 
            { 
                entries = value;
                RaisePropertyChanged("Entries");
            }
        }
        #endregion

        #region ExpensesMainViewModel()
        public ExpensesMainViewModel()
        {
            expenseRepository = new ExpenseRepository();
            categoryRepository = new CategoryRepository();

            entries = new ObservableCollection<Expense>();
        } 
        #endregion

        #region DataLoadAsync()
        public async Task DataLoadAsync()
        {
            try
            {
                List<Expense> items = await expenseRepository.GetListAsync();
                int categoryType = 0;
                Int32.TryParse(TransactionType.Expense.ToString("D"), out categoryType);
                List<Category> categories = await categoryRepository.GetListAsync(categoryType);

                foreach (Expense item in items)
                {
                    Category category = categories.FirstOrDefault(i => i.Id == item.CategoryId);
                    item.CategoryName = category != null ? category.Name : "-";
                    entries.Add(item);
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region GoToEditEntityPage()
        public void GoToEditEntityPage(int id)
        {
            NavigateService.Instance.Navigate(typeof(ExpenseEditPage), id);
        } 
        #endregion
    }
}
