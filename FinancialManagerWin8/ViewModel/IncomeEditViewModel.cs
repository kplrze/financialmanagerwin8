﻿using FinancialManagerWin8.Common;
using FinancialManagerWin8.DataAccessLayer;
using FinancialManagerWin8.Model;
using FinancialManagerWin8.Model.Enums;
using FinancialManagerWin8.View;
using FinancialManagerWin8.View.Incomes;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FinancialManagerWin8.ViewModel
{
    public class IncomeEditViewModel : VMBase
    {
        #region Properties & fields
        private IRepository<Income> incomeRepository;
        private ICategoryRepository categoryRepository;

        public ICommand SaveCmd { get; private set; }
        public ICommand DeleteCmd { get; set; }
        public ICommand GoToIncomesMainPageCmd { get; set; }

        #region RecordId
        private int recordId = 0;
        public int RecordId
        {
            get { return recordId; }
            set { recordId = value; }
        }
        #endregion

        #region SelectedCategory
        private int selectedCategory = 0;
        public int SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                if (selectedCategory == value)
                {
                    return;
                }
                selectedCategory = value;
                RaisePropertyChanged("SelectedCategory");
            }
        }
        #endregion

        #region Name
        private string name = String.Empty;
        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                {
                    return;
                }
                name = value;
                RaisePropertyChanged("Name");
            }
        }
        #endregion

        #region Amount
        private string amount = String.Empty;
        public string Amount
        {
            get { return amount.Replace('.', ','); }
            set
            {
                if (amount == value)
                {
                    return;
                }
                amount = value;
                RaisePropertyChanged("Amount");
            }
        }
        #endregion

        #region DateCreated
        private DateTime dateCreated;
        public DateTime DateCreated
        {
            get
            {
                return dateCreated == DateTime.MinValue ? DateTime.Now : dateCreated;
            }
            set
            {
                if (dateCreated == value)
                {
                    return;
                }
                dateCreated = value;
                RaisePropertyChanged("DateCreated");
            }
        }
        #endregion

        #region Details
        private string details = String.Empty;
        public string Details
        {
            get { return details; }
            set
            {
                if (details == value)
                {
                    return;
                }
                details = value;
                RaisePropertyChanged("Details");
            }
        }
        #endregion

        #region CategoryId
        private int categoryId = 0;
        public int CategoryId
        {
            get { return categoryId; }
            set
            {
                if (categoryId == value)
                {
                    return;
                }
                categoryId = value;
                RaisePropertyChanged("CategoryId");
            }
        }
        #endregion

        #region Categories
        private ObservableCollection<CollectionItemVM> categories = new ObservableCollection<CollectionItemVM>();
        public ObservableCollection<CollectionItemVM> Categories
        {
            get { return categories; }
        }
        #endregion

        #region AddingElementsVisibility
        private bool addingElementsVisibility = false;
        public bool AddingElementsVisibility
        {
            get { return addingElementsVisibility; }
            set
            {
                if (addingElementsVisibility == value)
                {
                    return;
                }
                
                addingElementsVisibility = value;
                RaisePropertyChanged("AddingElementsVisibility");
            }
        } 
        #endregion

        #endregion

        #region IncomeEditViewModel()
        public IncomeEditViewModel()
        {
            incomeRepository = new IncomeRepository();
            categoryRepository = new CategoryRepository();

            SaveCmd = new RelayCommand(SaveAsync);
            DeleteCmd = new RelayCommand(DeleteAsync);
            GoToIncomesMainPageCmd = new RelayCommand(GoToIncomesMainPage);
        } 
        #endregion

        #region InitAsync()
        public async Task InitAsync()
        {
            try
            {
                int categoryType = 0;
                Int32.TryParse(TransactionType.Income.ToString("D"), out categoryType);
                if (categoryType > 0)
                {
                    List<Category> incomesCategories = await categoryRepository.GetListAsync(categoryType);

                    foreach (Category incomeCategory in incomesCategories)
                    {
                        categories.Add(new CollectionItemVM(incomeCategory.Name, incomeCategory.Id));
                    }
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
        }
        #endregion

        #region SaveAsync()
        private async void SaveAsync()
        {
            try
            {
                if(String.IsNullOrEmpty(Name))
                {
                    var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                    string message = loader.GetString("msgCompleteRequiredField");

                    await ShowMessageDialogAsync(VMMessageType.Information, message);
                    return;
                }

                if (RecordId > 0)
                {
                    Income income = await incomeRepository.GetByIdAsync(RecordId);
                    if (income != null)
                    {
                        UpdateRecordByUI(ref income);
                        await incomeRepository.UpdateAsync(income);
                    }
                }
                else
                {
                    Income newIncome = new Income();

                    UpdateRecordByUI(ref newIncome);
                    await incomeRepository.AddAsync(newIncome);
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Save);
            GoToIncomesMainPage();
        }
        #endregion

        #region DataLoadAsync()
        public async Task DataLoadAsync()
        {
            try
            {
                AddingElementsVisibility = (RecordId > 0);

                if (RecordId > 0)
                {
                    Income income = await incomeRepository.GetByIdAsync(RecordId);
                    if (income != null)
                    {
                        Name = income.Name;
                        Amount = income.Amount.ToString();
                        DateCreated = income.DateCreated;
                        SelectedCategory = income.CategoryId;
                        Details = income.Details;
                    }
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        }
        #endregion

        #region DeleteAsync();
        private async void DeleteAsync()
        {
            try
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                string deleteQuestion = loader.GetString("MessageDialog_DeleteQuestion");

                await ShowMessageDialogAsync(VMMessageType.Question, deleteQuestion);
                //Reszta akcji w implementacjach metod MessageDialog
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Delete);
        }
        #endregion

        #region MessageDialogQuestionYes_Click()
        protected async override void MessageDialogQuestionYes_Click(Windows.UI.Popups.IUICommand command)
        {
            if (RecordId > 0)
            {
                try
                {
                    await incomeRepository.DeleteAsync(RecordId);
                    GoToIncomesMainPage();
                }
                catch (Exception ex)
                {
                    VMException = ex;
                    //Zapis wyjątku warstwa wyżej
                }
            }
        }
        #endregion

        #region MessageDialogQuestionNo_Click()
        protected override void MessageDialogQuestionNo_Click(Windows.UI.Popups.IUICommand command)
        {
            //Bez akcji
        }
        #endregion

        #region GoToIncomesMainPage()
        private void GoToIncomesMainPage()
        {
            NavigateService.Instance.Navigate(typeof(IncomesMainPage), typeof(MasterPage));
        }
        #endregion

        #region UpdateRecordByUI()
        private void UpdateRecordByUI(ref Income income)
        {
            income.Name = Name;

            decimal amount = 0;
            Decimal.TryParse(Amount, out amount);

            income.Amount = amount;
            income.DateCreated = DateCreated;
            income.CategoryId = SelectedCategory;
            income.Details = Details;
        }
        #endregion
    }
}
