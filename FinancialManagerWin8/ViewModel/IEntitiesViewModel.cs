﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManagerWin8.ViewModel
{
    public interface IEntitiesViewModel
    {
        void GoToEditEntityPage(int id);
    }
}
