﻿namespace FinancialManagerWin8.ViewModel
{
    public class CollectionItemVM
    {
        public string Name { get; set; }
        public int Value { get; set; }

        public CollectionItemVM(string name, int value)
        {
            this.Name = name;
            this.Value = value;
        }
    }
}
