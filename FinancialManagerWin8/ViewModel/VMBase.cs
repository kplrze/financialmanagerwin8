﻿using FinancialManagerWin8.Common;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace FinancialManagerWin8.ViewModel
{
    public class VMBase : ViewModelBase
    {
        protected Exception VMException { get; set; }

        #region EmptyContentText()
        private string emptyContentText = String.Empty;
        public string EmptyContentText
        {
            get { return emptyContentText; }
            set
            {
                if (emptyContentText == value)
                {
                    return;
                }
                emptyContentText = value;
                RaisePropertyChanged("EmptyContentText");
            }
        } 
        #endregion

        #region VMBase()
        public VMBase()
        {
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            EmptyContentText = loader.GetString("global_EmptyContentText");
        } 
        #endregion

        #region SaveExceptionIfExists()
        protected async Task SaveExceptionIfExists()
        {
            if (VMException != null)
            {
                await ErrorLogHelper.Save(VMException);
            }
        } 
        #endregion

        #region ResultActionAsync()
        protected async Task ResultActionAsync(VMActionType actionType)
        {
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

            if (VMException != null)
            {
                switch (actionType)
                {
                    case VMActionType.Save:

                        break;
                    case VMActionType.Update:
                        
                        break;
                    case VMActionType.Delete:
                        string msgDeleteError = loader.GetString("msgDeleteError");
                        await ShowMessageDialogAsync(VMMessageType.Information, msgDeleteError);
                        break;
                    case VMActionType.Load:
                        string msgLoadError = loader.GetString("msgLoadError");
                        await ShowMessageDialogAsync(VMMessageType.Information, msgLoadError);
                        break;
                }
            }
            else
            {
                switch (actionType)
                {
                    case VMActionType.Save:

                        break;
                    case VMActionType.Update:

                        break;
                    case VMActionType.Delete:
                        string mgsDeleteOK = loader.GetString("mgsDeleteOK");
                        await ShowMessageDialogAsync(VMMessageType.Information, mgsDeleteOK);
                        break;
                    case VMActionType.Load:
                        //brak akcji
                        break;
                }
            }
        }
        #endregion

        #region ShowMessageDialogAsync()
        protected async Task ShowMessageDialogAsync(VMMessageType messageType, string message)
        {
            MessageDialog msg = new Windows.UI.Popups.MessageDialog(message);

            switch (messageType)
            {
                case VMMessageType.Question:

                    var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                    string yes = loader.GetString("MessageDialog_QuestionYes");
                    string no = loader.GetString("MessageDialog_QuestionNo");

                    msg.Commands.Add(new Windows.UI.Popups.UICommand(yes, MessageDialogQuestionYes_Click));
                    msg.Commands.Add(new Windows.UI.Popups.UICommand(no, MessageDialogQuestionNo_Click));
                    break;
            }

            await msg.ShowAsync();
        } 
        #endregion

        #region MessageDialogQuestionNo_Click()
        protected virtual void MessageDialogQuestionNo_Click(IUICommand command)
        {
            throw new NotImplementedException();
        } 
        #endregion

        #region MessageDialogQuestionYes_Click()
        protected virtual void MessageDialogQuestionYes_Click(IUICommand command)
        {
            throw new NotImplementedException();
        } 
        #endregion
    }

    public enum VMActionType
    {
        Save,
        Update,
        Delete,
        Load
    }

    public enum VMMessageType
    {
        Information,
        Question
    }
}
