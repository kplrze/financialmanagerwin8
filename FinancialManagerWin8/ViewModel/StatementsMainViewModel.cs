﻿using FinancialManagerWin8.Common;
using FinancialManagerWin8.Common.Telerik;
using FinancialManagerWin8.DataAccessLayer;
using FinancialManagerWin8.Model;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Telerik.UI.Xaml.Controls.Chart;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace FinancialManagerWin8.ViewModel
{
    public class StatementsMainViewModel : VMBase
    {
        #region Properties & Fields

        private IExpenseRepository expenseRepository;
        private IIncomeRepository incomeRepository;
        private IRepository<Category> categoryRepository;

        private int currentYear = DateTime.Now.Year;
        private int lastYear = DateTime.Now.Year - 1;

        public ICommand ChangeDataContextTopChartCmd { get; private set; }

        #region FirstBarSeriesItems
        private ObservableCollection<BarSeriesItem> firstBarSeriesItems = null;
        public ObservableCollection<BarSeriesItem> FirstBarSeriesItems
        {
            get { return firstBarSeriesItems; }
            private set
            {
                firstBarSeriesItems = value;
                RaisePropertyChanged("FirstBarSeriesItems");
            }
        }
        #endregion

        #region SecondBarSeriesItems
        private ObservableCollection<BarSeriesItem> secondBarSeriesItems = null;
        public ObservableCollection<BarSeriesItem> SecondBarSeriesItems
        {
            get { return secondBarSeriesItems; }
            private set
            {
                secondBarSeriesItems = value;
                RaisePropertyChanged("SecondBarSeriesItems");
            }
        }
        #endregion

        #region MenuExpenseValue
        private string menuExpenseValue = "0";
        public string MenuExpenseValue
        {
            get { return menuExpenseValue; }
            set
            {
                if (menuExpenseValue == value)
                {
                    return;
                }
                menuExpenseValue = value;
                RaisePropertyChanged("MenuExpenseValue");
            }
        }
        #endregion

        #region MenuIncomesValue
        private string menuIncomesValue = "0";
        public string MenuIncomesValue
        {
            get { return menuIncomesValue; }
            set
            {
                if (menuIncomesValue == value)
                {
                    return;
                }
                menuIncomesValue = value;
                RaisePropertyChanged("MenuIncomesValue");
            }
        }
        #endregion

        #region MenuIncomesAPValue
        private string menuIncomesAPValue = String.Empty;
        public string MenuIncomesAPValue
        {
            get { return menuIncomesAPValue; }
            set
            {
                if (menuIncomesAPValue == value)
                {
                    return;
                }
                menuIncomesAPValue = value;
                RaisePropertyChanged("MenuIncomesAPValue");
            }
        }
        #endregion

        #region MenuExpenseAPValue
        private string menuExpenseAPValue = String.Empty;
        public string MenuExpenseAPValue
        {
            get { return menuExpenseAPValue; }
            set
            {
                if (menuExpenseAPValue == value)
                {
                    return;
                }
                menuExpenseAPValue = value;
                RaisePropertyChanged("MenuExpenseAPValue");
            }
        }
        #endregion

        #region MenuExpenseAPValueColor
        private SolidColorBrush menuExpenseAPValueColor;
        public SolidColorBrush MenuExpenseAPValueColor
        {
            get { return menuExpenseAPValueColor; }
            set
            {
                if (menuExpenseAPValueColor == value)
                {
                    return;
                }
                menuExpenseAPValueColor = value;
                RaisePropertyChanged("MenuExpenseAPValueColor");
            }
        }
        #endregion

        #region MenuIncomesAPValueColor
        private SolidColorBrush menuIncomesAPValueColor;
        public SolidColorBrush MenuIncomesAPValueColor
        {
            get { return menuIncomesAPValueColor; }
            set
            {
                if (menuIncomesAPValueColor == value)
                {
                    return;
                }
                menuIncomesAPValueColor = value;
                RaisePropertyChanged("MenuIncomesAPValueColor");
            }
        }
        #endregion

        #region Visibility expenses triangles
        private bool visibilityExpensesGreenTriangle = false;
        public bool VisibilityExpensesGreenTriangle
        {
            get { return visibilityExpensesGreenTriangle; }
            set
            {
                if (visibilityExpensesGreenTriangle == value)
                {
                    return;
                }
                visibilityExpensesGreenTriangle = value;
                RaisePropertyChanged("VisibilityExpensesGreenTriangle");
                RaisePropertyChanged("VisibilityExpensesRedTriangle");
            }
        }

        public bool VisibilityExpensesRedTriangle
        {
            get { return !visibilityExpensesGreenTriangle; }
        }
        #endregion

        #region Visibility incomes triangles
        private bool visibilityincomesGreenTriangle = false;
        public bool VisibilityIncomesGreenTriangle
        {
            get { return visibilityincomesGreenTriangle; }
            set
            {
                if (visibilityincomesGreenTriangle == value)
                {
                    return;
                }
                visibilityincomesGreenTriangle = value;
                RaisePropertyChanged("VisibilityIncomesGreenTriangle");
                RaisePropertyChanged("VisibilityIncomesRedTriangle");
            }
        }

        public bool VisibilityIncomesRedTriangle
        {
            get { return !visibilityincomesGreenTriangle; }
        }
        #endregion

        #region ExpensesTOP5InCategoriesItems
        private ObservableCollection<ExpensesInCategoriesItem> expensesTOP5InCategoriesItems = null;
        public ObservableCollection<ExpensesInCategoriesItem> ExpensesTOP5InCategoriesItems
        {
            get { return expensesTOP5InCategoriesItems; }
            private set
            {
                expensesTOP5InCategoriesItems = value;
                RaisePropertyChanged("ExpensesTOP5InCategoriesItems");
            }
        }
        #endregion

        #region ExpensesTOP10InCategoriesItems
        private ObservableCollection<ExpensesInCategoriesItem> expensesTOP10InCategoriesItems = null;
        public ObservableCollection<ExpensesInCategoriesItem> ExpensesTOP10InCategoriesItems
        {
            get { return expensesTOP10InCategoriesItems; }
            private set
            {
                expensesTOP10InCategoriesItems = value;
                RaisePropertyChanged("ExpensesTOP10InCategoriesItems");
            }
        }
        #endregion 

        #endregion

        #region StatementsMainViewModel()
        public StatementsMainViewModel()
        {
            expenseRepository = new ExpenseRepository();
            incomeRepository = new IncomeRepository();
            categoryRepository = new CategoryRepository();

            ChangeDataContextTopChartCmd = new RelayCommand<int>(ChangeDataContextTopChart);

            menuExpenseAPValueColor = new SolidColorBrush(Colors.Black);
        } 
        #endregion

        #region Commands methods
        private async void ChangeDataContextTopChart(int index)
        {
            if (FirstBarSeriesItems != null)
            {
                FirstBarSeriesItems.Clear();
            }

            if (SecondBarSeriesItems != null)
            {
                SecondBarSeriesItems.Clear();
            }

            switch (index)
            {
                case 0: //Wydatki
                    await LoadExpensesForTopChartAsync();
                    break;
                case 1: //Wydatki obecnego i poprzedniego roku
                    await LoadExpensesAPForTopChartAsync();
                    break;
                case 2: //Przychody
                    await LoadIncomesForTopChartAsync();
                    break;
                case 3: //Przychody obecnego i poprzedniego roku
                    await LoadIncomesAPForTopChartAsync();
                    break;
            }
        }
        #endregion

        #region LoadExpensesForTopChartAsync()
        private async Task LoadExpensesForTopChartAsync()
        {
            try
            {
                string[] months = AppHelpers.GetMonths(false);

                List<Expense> currentYearExpenses = await expenseRepository.GetListAsync(currentYear);

                if (currentYearExpenses != null && currentYearExpenses.Any())
                {
                    FirstBarSeriesItems = new ObservableCollection<BarSeriesItem>();

                    for (int month = 0; month < 12; month++)
                    {
                        BarSeriesItem barSeriesItem = new BarSeriesItem();
                        barSeriesItem.Text = months[month];
                        barSeriesItem.Value = currentYearExpenses.Where(i => i.DateCreated.Month == (month + 1)).Sum(i => i.Amount);

                        FirstBarSeriesItems.Add(barSeriesItem);
                    }
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region LoadExpensesAPForTopChartAsync()
        private async Task LoadExpensesAPForTopChartAsync()
        {
            try
            {
                string[] months = AppHelpers.GetMonths(false);

                List<Expense> currentYearExpenses = await expenseRepository.GetListAsync(currentYear);

                if (currentYearExpenses != null && currentYearExpenses.Any())
                {
                    FirstBarSeriesItems = new ObservableCollection<BarSeriesItem>();

                    for (int month = 0; month < 12; month++)
                    {
                        BarSeriesItem barSeriesItem = new BarSeriesItem();
                        barSeriesItem.Text = months[month];
                        barSeriesItem.Value = currentYearExpenses.Where(i => i.DateCreated.Month == (month + 1)).Sum(i => i.Amount);

                        FirstBarSeriesItems.Add(barSeriesItem);
                    }
                }

                List<Expense> lastYearExpenses = await expenseRepository.GetListAsync(lastYear);

                if (lastYearExpenses != null && lastYearExpenses.Any())
                {
                    SecondBarSeriesItems = new ObservableCollection<BarSeriesItem>();

                    for (int month = 0; month < 12; month++)
                    {
                        BarSeriesItem barSeriesItem = new BarSeriesItem();
                        barSeriesItem.Text = months[month];
                        barSeriesItem.Value = lastYearExpenses.Where(i => i.DateCreated.Month == (month + 1)).Sum(i => i.Amount);

                        SecondBarSeriesItems.Add(barSeriesItem);
                    }
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region LoadIncomesForTopChartAsync()
        private async Task LoadIncomesForTopChartAsync()
        {
            try
            {
                string[] months = AppHelpers.GetMonths(false);

                List<Income> currentYearIncomes = await incomeRepository.GetListAsync(currentYear);

                if (currentYearIncomes != null && currentYearIncomes.Any())
                {
                    FirstBarSeriesItems = new ObservableCollection<BarSeriesItem>();

                    for (int month = 0; month < 12; month++)
                    {
                        BarSeriesItem barSeriesItem = new BarSeriesItem();
                        barSeriesItem.Text = months[month];
                        barSeriesItem.Value = currentYearIncomes.Where(i => i.DateCreated.Month == (month + 1)).Sum(i => i.Amount);

                        FirstBarSeriesItems.Add(barSeriesItem);
                    }
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region LoadIncomesAPForTopChartAsync()
        private async Task LoadIncomesAPForTopChartAsync()
        {
            try
            {
                string[] months = AppHelpers.GetMonths(false);

                List<Income> currentYearIncomes = await incomeRepository.GetListAsync(currentYear);

                if (currentYearIncomes != null && currentYearIncomes.Any())
                {
                    FirstBarSeriesItems = new ObservableCollection<BarSeriesItem>();

                    for (int month = 0; month < 12; month++)
                    {
                        BarSeriesItem barSeriesItem = new BarSeriesItem();
                        barSeriesItem.Text = months[month];
                        barSeriesItem.Value = currentYearIncomes.Where(i => i.DateCreated.Month == (month + 1)).Sum(i => i.Amount);

                        FirstBarSeriesItems.Add(barSeriesItem);
                    }
                }

                List<Income> lastYearIncomes = await incomeRepository.GetListAsync(lastYear);

                if (lastYearIncomes != null && lastYearIncomes.Any())
                {
                    SecondBarSeriesItems = new ObservableCollection<BarSeriesItem>();

                    for (int month = 0; month < 12; month++)
                    {
                        BarSeriesItem barSeriesItem = new BarSeriesItem();
                        barSeriesItem.Text = months[month];
                        barSeriesItem.Value = lastYearIncomes.Where(i => i.DateCreated.Month == (month + 1)).Sum(i => i.Amount);

                        SecondBarSeriesItems.Add(barSeriesItem);
                    }
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region DataLoadAsync()
        public async Task DataLoadAsync()
        {
            try
            {
                decimal currentYearExpenseValue = 0.0m;
                decimal lastYearExpensesValue = 0.0m;

                decimal currentYearIncomesValue = 0.0m;
                decimal lastYearIncomesValue = 0.0m;

                List<Expense> currentYearExpenses = await expenseRepository.GetListAsync(currentYear);
                if(currentYearExpenses != null && currentYearExpenses.Any())
                {
                    currentYearExpenseValue = currentYearExpenses.Sum(i => i.Amount);
                }

                List<Expense> lastYearExpenses = await expenseRepository.GetListAsync(lastYear);
                if (lastYearExpenses != null && lastYearExpenses.Any())
                {
                    lastYearExpensesValue = lastYearExpenses.Sum(i => i.Amount);
                }

                List<Income> currentYearIncomes = await incomeRepository.GetListAsync(currentYear);
                if(currentYearIncomes != null && currentYearIncomes.Any())
                {
                    currentYearIncomesValue = currentYearIncomes.Sum(i => i.Amount);
                }

                List<Income> lastYearIncomes = await incomeRepository.GetListAsync(lastYear);
                if(lastYearIncomes != null && lastYearIncomes.Any())
                {
                    lastYearIncomesValue = lastYearIncomes.Sum(i => i.Amount);
                }


                MenuExpenseValue = currentYearExpenseValue.ToString("0.00");
                MenuIncomesValue = currentYearIncomesValue.ToString("0.00");

                //MenuExpenseAPValue
                if(currentYearExpenseValue > lastYearExpensesValue)
                {
                    //Jeśli nie było wydatków w poprzednim roku, to nastąpił wzrost o 100% w stosunku do obecnego (i nie dzielimy przez 0)
                    decimal result = lastYearExpensesValue == 0 ? 100 : ((currentYearExpenseValue - lastYearExpensesValue) / lastYearExpensesValue) * 100;
                    MenuExpenseAPValueColor = new SolidColorBrush(Colors.Red);
                    MenuExpenseAPValue = result.ToString("0.00") + " %";
                    VisibilityExpensesGreenTriangle = true;
                }
                else
                {
                    decimal result = currentYearExpenseValue == 0 ? 100 : ((currentYearExpenseValue - lastYearExpensesValue) / currentYearExpenseValue) * 100;
                    MenuExpenseAPValueColor = new SolidColorBrush(Colors.Green);
                    MenuExpenseAPValue = (result*(-1)).ToString("0.00") + " %";
                    VisibilityExpensesGreenTriangle = false;
                }

                //MenuIncomesAPValue
                if (currentYearIncomesValue > lastYearIncomesValue)
                {
                    decimal result = lastYearIncomesValue == 0 ? 100 : ((currentYearIncomesValue - lastYearIncomesValue) / lastYearIncomesValue) * 100;
                    MenuIncomesAPValueColor = new SolidColorBrush(Colors.Green);
                    MenuIncomesAPValue = result.ToString("0.00") + " %";
                    VisibilityIncomesGreenTriangle = true;
                }
                else
                {
                    decimal result = currentYearIncomesValue == 0 ? 100 : ((currentYearIncomesValue - lastYearIncomesValue) / currentYearIncomesValue) * 100;
                    MenuIncomesAPValueColor = new SolidColorBrush(Colors.Red);
                    MenuIncomesAPValue = (result * (-1)).ToString("0.00") + " %";
                    VisibilityIncomesGreenTriangle = false;
                }

                ChangeDataContextTopChart(0);

                var topCategoriesOfExp = (from item in currentYearExpenses
                                           where item.CategoryId > 0
                                           group item by item.CategoryId into g
                                           select new { CategoryId = g.Key, Amount = g.Sum(i => i.Amount) }).OrderByDescending(a => a.Amount);

                List<Category> categories = await categoryRepository.GetListAsync();

                ExpensesTOP5InCategoriesItems = new ObservableCollection<ExpensesInCategoriesItem>();

                foreach(var categoryOfExp in topCategoriesOfExp.Take(5))
                {
                    Category expCategory = categories.FirstOrDefault(i => i.Id == categoryOfExp.CategoryId);
                    if(expCategory != null)
                    {
                        ExpensesInCategoriesItem expCategoryItem = new ExpensesInCategoriesItem();
                        expCategoryItem.CategoryName = expCategory.Name;
                        expCategoryItem.Amount = categoryOfExp.Amount;
                        if(currentYearExpenseValue > 0)
                        {
                            expCategoryItem.PercentOfTotal = (categoryOfExp.Amount / currentYearExpenseValue) * 100;
                        }

                        ExpensesTOP5InCategoriesItems.Add(expCategoryItem);
                    }
                }

                ExpensesTOP10InCategoriesItems = new ObservableCollection<ExpensesInCategoriesItem>();

                foreach (var categoryOfExp in topCategoriesOfExp.Take(10))
                {
                    Category expCategory = categories.FirstOrDefault(i => i.Id == categoryOfExp.CategoryId);
                    if (expCategory != null)
                    {
                        ExpensesInCategoriesItem expCategoryItem = new ExpensesInCategoriesItem();
                        expCategoryItem.CategoryName = expCategory.Name;
                        expCategoryItem.Amount = categoryOfExp.Amount;
                        ExpensesTOP10InCategoriesItems.Add(expCategoryItem);
                    }
                }

            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        }
        #endregion
    }
}
