﻿using FinancialManagerWin8.View.Categories;
using FinancialManagerWin8.Common;
using FinancialManagerWin8.Model;
using FinancialManagerWin8.Model.Enums;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FinancialManagerWin8.View;
using FinancialManagerWin8.DataAccessLayer;

namespace FinancialManagerWin8.ViewModel
{
    public class CategoryEditViewModel : VMBase
    {
        #region Properties & fields
        private IRepository<Category> repository;

        public ICommand SaveCmd { get; private set; }
        public ICommand DeleteCmd { get; set; }
        public ICommand GoToCategoriesMainPageCmd { get; set; }

        #region RecordId
        private int recordId = 0;
        public int RecordId
        {
            get { return recordId; }
            set { recordId = value; }
        } 
        #endregion

        #region Types
        private ObservableCollection<CollectionItemVM> types = null;
        public ObservableCollection<CollectionItemVM> Types
        {
            get
            {
                return types != null ? types : new ObservableCollection<CollectionItemVM>();
            }
        } 
        #endregion

        #region SelectedType
        private int selectedType = 0;
        public int SelectedType
        {
            get { return selectedType; }
            set
            {
                if (selectedType == value)
                {
                    return;
                }
                selectedType = value;
                RaisePropertyChanged("SelectedType");
            }
        } 
        #endregion

        #region Name
        private string name = String.Empty;
        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                {
                    return;
                }
                name = value;
                RaisePropertyChanged("Name");
            }
        } 
        #endregion

        #region AddingElementsVisibility
        private bool addingElementsVisibility = false;
        public bool AddingElementsVisibility
        {
            get { return addingElementsVisibility; }
            set
            {
                if (addingElementsVisibility == value)
                {
                    return;
                }

                addingElementsVisibility = value;
                RaisePropertyChanged("AddingElementsVisibility");
            }
        }
        #endregion

        #endregion

        #region CategoryEditViewModel()
        public CategoryEditViewModel()
        {
            repository = new CategoryRepository();

            SaveCmd = new RelayCommand(SaveAsync);
            DeleteCmd = new RelayCommand(DeleteAsync);
            GoToCategoriesMainPageCmd = new RelayCommand(GoToCategoriesMainPage);

            types = new ObservableCollection<CollectionItemVM>();
        } 
        #endregion

        #region Init()
        public async Task InitAsync()
        {
            try
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                foreach (Enum transType in Enum.GetValues(typeof(TransactionType)))
                {
                    string name = loader.GetString("TransactionType_" + transType.ToString("G"));

                    types.Add(new CollectionItemVM(name, Convert.ToInt32(transType.ToString("D"))));
                }
            }
            catch(Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
        } 
        #endregion

        #region DataLoadAsync()
        public async Task DataLoadAsync()
        {
            try
            {
                AddingElementsVisibility = (RecordId > 0);

                if (RecordId > 0)
                {
                    Category category = await repository.GetByIdAsync(RecordId);
                    if (category != null)
                    {
                        Name = category.Name;
                        SelectedType = category.Type;
                    }
                }
            }
            catch(Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region SaveAsync()
        private async void SaveAsync()
        {
            try
            {
                if (String.IsNullOrEmpty(Name))
                {
                    var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                    string message = loader.GetString("msgCompleteRequiredField");

                    await ShowMessageDialogAsync(VMMessageType.Information, message);
                    return;
                }

                if (String.IsNullOrEmpty(Name))
                {
                    var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                    string message = loader.GetString("msgCompleteRequiredField");

                    await ShowMessageDialogAsync(VMMessageType.Information, message);
                    return;
                }

                if (RecordId > 0)
                {
                    Category category = await repository.GetByIdAsync(RecordId);
                    if (category != null)
                    {
                        UpdateRecordByUI(ref category);
                        
                        await repository.UpdateAsync(category);

                    }
                }
                else
                {
                    Category newCategory = new Category();
                    UpdateRecordByUI(ref newCategory);
                    await repository.AddAsync(newCategory);
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Save);
            GoToCategoriesMainPage();
        } 
        #endregion

        #region DeleteAsync();
        private async void DeleteAsync()
        {
            try
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                string deleteQuestion = loader.GetString("MessageDialog_DeleteQuestion");

                await ShowMessageDialogAsync(VMMessageType.Question, deleteQuestion);
                //Reszta akcji w implementacjach metod MessageDialog
            }
            catch(Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Delete);
        } 
        #endregion

        #region MessageDialogQuestionYes_Click()
        protected async override void MessageDialogQuestionYes_Click(Windows.UI.Popups.IUICommand command)
        {
            if (RecordId > 0)
            {
                try
                {
                    await repository.DeleteAsync(RecordId);
                    GoToCategoriesMainPage();
                }
                catch (Exception ex)
                {
                    VMException = ex;
                }
            }
        } 
        #endregion

        #region MessageDialogQuestionNo_Click()
        protected override void MessageDialogQuestionNo_Click(Windows.UI.Popups.IUICommand command)
        {
            //Bez akcji
        } 
        #endregion

        #region GoToCategoriesMainPage()
        private void GoToCategoriesMainPage()
        {
            NavigateService.Instance.Navigate(typeof(CategoriesMainPage), typeof(MasterPage));
        } 
        #endregion

        #region UpdateRecordByUI()
        private void UpdateRecordByUI(ref Category category)
        {
            category.Name = Name;
            category.Type = SelectedType;
        } 
        #endregion
    }
}
