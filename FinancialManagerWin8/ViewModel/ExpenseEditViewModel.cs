﻿using FinancialManagerWin8.Common;
using FinancialManagerWin8.DataAccessLayer;
using FinancialManagerWin8.Model;
using FinancialManagerWin8.Model.Enums;
using FinancialManagerWin8.View;
using FinancialManagerWin8.View.Expenses;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;

namespace FinancialManagerWin8.ViewModel
{
    public class ExpenseEditViewModel : VMBase
    {
        #region Properties & fields
        private IRepository<Expense> expenseRepository;
        private ICategoryRepository categoryRepository;

        public ICommand SaveCmd { get; private set; }
        public ICommand DeleteCmd { get; set; }
        public ICommand GoToExpensesMainPageCmd { get; set; }

        #region RecordId
        private int recordId = 0;
        public int RecordId
        {
            get { return recordId; }
            set { recordId = value; }
        } 
        #endregion

        #region SelectedCategory
        private int selectedCategory = 0;
        public int SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                if (selectedCategory == value)
                {
                    return;
                }
                selectedCategory = value;
                RaisePropertyChanged("SelectedCategory");
            }
        }
        #endregion

        #region Name
        private string name = String.Empty;
        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                {
                    return;
                }
                name = value;
                RaisePropertyChanged("Name");
            }
        }
        #endregion

        #region Amount
        private string amount = String.Empty;
        public string Amount
        {
            get { return amount.Replace('.', ','); }
            set 
            {
                if(amount == value)
                {
                    return;
                }
                amount = value;
                RaisePropertyChanged("Amount");
            }
        }
        #endregion

        #region DateCreated
        private DateTime dateCreated;
        public DateTime DateCreated
        {
            get 
            {
                return dateCreated == DateTime.MinValue ? DateTime.Now : dateCreated; 
            }
            set
            {
                if(dateCreated == value)
                {
                    return;
                }
                dateCreated = value;
                RaisePropertyChanged("DateCreated");
            }
        }
        #endregion

        #region Details
        private string details = String.Empty;
        public string Details
        {
            get { return details; }
            set
            {
                if (details == value)
                {
                    return;
                }
                details = value;
                RaisePropertyChanged("Details");
            }
        }
        #endregion

        #region CategoryId
        private int categoryId = 0;
        public int CategoryId
        {
            get { return categoryId; }
            set
            {
                if (categoryId == value)
                {
                    return;
                }
                categoryId = value;
                RaisePropertyChanged("CategoryId");
            }
        }
        #endregion

        #region Categories
        private ObservableCollection<CollectionItemVM> categories = new ObservableCollection<CollectionItemVM>();
        public ObservableCollection<CollectionItemVM> Categories
        {
            get { return categories; }
        }
        #endregion

        #region AddingElementsVisibility
        private bool addingElementsVisibility = false;
        public bool AddingElementsVisibility
        {
            get { return addingElementsVisibility; }
            set
            {
                if (addingElementsVisibility == value)
                {
                    return;
                }

                addingElementsVisibility = value;
                RaisePropertyChanged("AddingElementsVisibility");
            }
        }
        #endregion

        #endregion

        #region ExpenseEditViewModel()
        public ExpenseEditViewModel()
        {
            expenseRepository = new ExpenseRepository();
            categoryRepository = new CategoryRepository();

            SaveCmd = new RelayCommand(SaveAsync);
            DeleteCmd = new RelayCommand(DeleteAsync);
            GoToExpensesMainPageCmd = new RelayCommand(GoToExpenseMainPage);
        } 
        #endregion

        #region SaveAsync()
        private async void SaveAsync()
        {
            try
            {
                if (String.IsNullOrEmpty(Name))
                {
                    var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                    string message = loader.GetString("msgCompleteRequiredField");

                    await ShowMessageDialogAsync(VMMessageType.Information, message);
                    return;
                }

                if (RecordId > 0)
                {
                    Expense expense = await expenseRepository.GetByIdAsync(RecordId);
                    if (expense != null)
                    {
                        UpdateRecordByUI(ref expense);
                        await expenseRepository.UpdateAsync(expense);
                    }
                }
                else
                {
                    Expense newExpense = new Expense();

                    UpdateRecordByUI(ref newExpense);
                    await expenseRepository.AddAsync(newExpense);
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Save);
            GoToExpenseMainPage();
        } 
        #endregion

        #region InitAsync()
        public async Task InitAsync()
        {
            try
            {
                int categoryType = 0;
                Int32.TryParse(TransactionType.Expense.ToString("D"), out categoryType);
                if(categoryType > 0)
                {
                    List<Category> expenseCategories = await categoryRepository.GetListAsync(categoryType);

                    foreach(Category expenseCategory in expenseCategories)
                    {
                        categories.Add(new CollectionItemVM(expenseCategory.Name, expenseCategory.Id));
                    }
                }
            }
            catch(Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
        }
        #endregion

        #region DataLoadAsync()
        public async Task DataLoadAsync()
        {
            try
            {
                AddingElementsVisibility = (RecordId > 0);

                if (RecordId > 0)
                {
                    Expense expense = await expenseRepository.GetByIdAsync(RecordId);
                    if (expense != null)
                    {
                        Name = expense.Name;
                        Amount = expense.Amount.ToString();
                        DateCreated = expense.DateCreated;
                        SelectedCategory = expense.CategoryId;
                        Details = expense.Details;
                    }
                }
            }
            catch(Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region DeleteAsync();
        private async void DeleteAsync()
        {
            try
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                string deleteQuestion = loader.GetString("MessageDialog_DeleteQuestion");

                await ShowMessageDialogAsync(VMMessageType.Question, deleteQuestion);
                //Reszta akcji w implementacjach metod MessageDialog
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Delete);
        }
        #endregion

        #region MessageDialogQuestionYes_Click()
        protected async override void MessageDialogQuestionYes_Click(Windows.UI.Popups.IUICommand command)
        {
            if (RecordId > 0)
            {
                try
                {
                    await expenseRepository.DeleteAsync(RecordId);
                    GoToExpenseMainPage();
                }
                catch (Exception ex)
                {
                    VMException = ex;
                    //Zapis wyjątku warstwa wyżej
                }
            }
        }
        #endregion

        #region MessageDialogQuestionNo_Click()
        protected override void MessageDialogQuestionNo_Click(Windows.UI.Popups.IUICommand command)
        {
            //Bez akcji
        }
        #endregion

        #region GoToExpenseMainPage()
        private void GoToExpenseMainPage()
        {
            NavigateService.Instance.Navigate(typeof(ExpensesMainPage), typeof(MasterPage));
        }
        #endregion

        #region UpdateRecordByUI()
        private void UpdateRecordByUI(ref Expense expense)
        {
            expense.Name = Name;

            decimal amount = 0;
            Decimal.TryParse(Amount, out amount);

            expense.Amount = amount;
            expense.DateCreated = DateCreated;
            expense.CategoryId = SelectedCategory;
            expense.Details = Details;
        }
        #endregion
    }
}
