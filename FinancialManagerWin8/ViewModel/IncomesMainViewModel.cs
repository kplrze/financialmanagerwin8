﻿using FinancialManagerWin8.Common;
using FinancialManagerWin8.DataAccessLayer;
using FinancialManagerWin8.Model;
using FinancialManagerWin8.Model.Enums;
using FinancialManagerWin8.View.Incomes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManagerWin8.ViewModel
{
    public class IncomesMainViewModel : VMBase, IEntitiesViewModel
    {
        #region Properties & fields
        private IRepository<Income> incomesRepository;
        private ICategoryRepository categoryRepository;

        private ObservableCollection<Income> entries = null;
        public ObservableCollection<Income> Entries
        {
            get { return entries; }
            private set { entries = value; }
        }
        #endregion

        #region IncomesMainViewModel()
        public IncomesMainViewModel()
        {
            incomesRepository = new IncomeRepository();
            categoryRepository = new CategoryRepository();

            entries = new ObservableCollection<Income>();
        } 
        #endregion

        #region DataLoadAsync()
        public async Task DataLoadAsync()
        {
            try
            {
                List<Income> items = await incomesRepository.GetListAsync();
                int categoryType = 0;
                Int32.TryParse(TransactionType.Income.ToString("D"), out categoryType);
                List<Category> categories = await categoryRepository.GetListAsync(categoryType);

                foreach (Income item in items)
                {
                    Category category = categories.FirstOrDefault(i => i.Id == item.CategoryId);
                    item.CategoryName = category != null ? category.Name : "-";
                    entries.Add(item);
                }
            }
            catch (Exception ex)
            {
                VMException = ex;
            }

            await SaveExceptionIfExists();
            await ResultActionAsync(VMActionType.Load);
        } 
        #endregion

        #region GoToEditEntityPage()
        public void GoToEditEntityPage(int id)
        {
            NavigateService.Instance.Navigate(typeof(IncomeEditPage), id);
        }
        #endregion
    }
}
