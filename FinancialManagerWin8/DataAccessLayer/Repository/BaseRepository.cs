﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace FinancialManagerWin8.DataAccessLayer
{
    public class BaseRepository
    {
        protected SQLiteAsyncConnection connection;
        
        protected async Task ConnectAsync()
        {
            if (connection == null)
            {
                StorageFile storageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Storage.db", CreationCollisionOption.OpenIfExists);
                string databaseFile = storageFile.Path;
                connection = new SQLiteAsyncConnection(databaseFile);
                
                //Tworzymy tabele jeśli nie istnieją
                await connection.CreateTableAsync<Model.Expense>();
                await connection.CreateTableAsync<Model.Income>();
                await connection.CreateTableAsync<Model.Category>();
            }
        }
    }
}
