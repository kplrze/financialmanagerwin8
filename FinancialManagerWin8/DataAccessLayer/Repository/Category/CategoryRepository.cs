﻿using FinancialManagerWin8.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManagerWin8.DataAccessLayer
{
    public class CategoryRepository : BaseRepository, ICategoryRepository
    {
        #region GetByIdAsync()
        public async Task<Category> GetByIdAsync(int id)
        {
            try
            {
                await ConnectAsync();

                return await connection.FindAsync<Category>(id);
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region GetListAsync()
        public async Task<List<Category>> GetListAsync()
        {
            try
            {
                await ConnectAsync();

                return await connection.Table<Category>().ToListAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<Category>> GetListAsync(int categoryType)
        {
            try
            {
                await ConnectAsync();

                return await connection.Table<Category>().Where(i => i.Type == categoryType).ToListAsync();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region AddAsync()
        public async Task AddAsync(Category entity)
        {
            try
            {
                await ConnectAsync();

                await connection.InsertAsync(entity);
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region DeleteAsync()
        public async Task DeleteAsync(int id)
        {
            try
            {
                await ConnectAsync();

                Category categoryToDelete = await GetByIdAsync(id);

                if (categoryToDelete != null)
                {
                    await connection.DeleteAsync(categoryToDelete);
                }
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region UpdateAsync()
        public async Task UpdateAsync(Category entity)
        {
            try
            {
                await ConnectAsync();

                await connection.UpdateAsync(entity);
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
