﻿using FinancialManagerWin8.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManagerWin8.DataAccessLayer
{
    public interface IExpenseRepository : IRepository<Expense>
    {
        Task<List<Expense>> GetListAsync(int year);
        Task<List<Expense>> GetListAsync(int year, int month);
    }
}
