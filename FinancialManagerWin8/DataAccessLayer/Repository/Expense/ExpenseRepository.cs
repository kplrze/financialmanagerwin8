﻿using FinancialManagerWin8.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinancialManagerWin8.DataAccessLayer
{
    public class ExpenseRepository : BaseRepository, IExpenseRepository
    {
        #region GetByIdAsync()
        public async Task<Expense> GetByIdAsync(int id)
        {
            try
            {
                await ConnectAsync();

                return await connection.FindAsync<Expense>(id);
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region GetListAsync()
        public async Task<List<Expense>> GetListAsync()
        {
            try
            {
                await ConnectAsync();

                return await connection.Table<Expense>().ToListAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<Expense>> GetListAsync(int year)
        {
            try
            {
                await ConnectAsync();

                DateTime dateFrom = new DateTime(year, 1, 1);
                DateTime dateTo = new DateTime(year, 12, 31);

                return await connection.Table<Expense>().Where(i => i.DateCreated >= dateFrom && i.DateCreated <= dateTo).ToListAsync();
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<Expense>> GetListAsync(int year, int month)
        {
            try
            {
                await ConnectAsync();

                int daysInMonth = DateTime.DaysInMonth(year, month);

                DateTime dateFrom = new DateTime(year, month, 1);
                DateTime dateTo = new DateTime(year, month, daysInMonth);

                return await connection.Table<Expense>().Where(i => i.DateCreated >= dateFrom && i.DateCreated <= dateTo).ToListAsync();
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region AddAsync()
        public async Task AddAsync(Expense entity)
        {
            try
            {
                await ConnectAsync();

                await connection.InsertAsync(entity);
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region DeleteAsync()
        public async Task DeleteAsync(int id)
        {
            try
            {
                await ConnectAsync();

                Expense expenseToDelete = await GetByIdAsync(id);

                if (expenseToDelete != null)
                {
                    await connection.DeleteAsync(expenseToDelete);
                }
            }
            catch
            {
                throw;
            }
        } 
        #endregion

        #region UpdateAsync()
        public async Task UpdateAsync(Expense entity)
        {
            try
            {
                await ConnectAsync();

                await connection.UpdateAsync(entity);
            }
            catch
            {
                throw;
            }
        } 
        #endregion
    }
}
