﻿using FinancialManagerWin8.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManagerWin8.DataAccessLayer
{
    public interface IIncomeRepository : IRepository<Income>
    {
        Task<List<Income>> GetListAsync(int year);
        Task<List<Income>> GetListAsync(int year, int month);
    }
}
