﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Data.Xml.Dom;
using Windows.Storage;

namespace FinancialManagerWin8.Common
{
    public class ErrorLogHelper
    {
        public static async Task Save(Exception ex)
        {
            try
            {
                string fileName = DateTime.Now.Year + "_" + DateTime.Now.Month + ".txt";

                StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);

                StringBuilder strBuilder = new StringBuilder();
                strBuilder.AppendLine("DateTime:" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
                strBuilder.AppendLine("Message:" + ex.Message);
                strBuilder.AppendLine("StackTrace:" + ex.StackTrace);
                strBuilder.AppendLine();

               await  FileIO.AppendTextAsync(file, strBuilder.ToString());
            }
            catch (Exception exception)
            {

            }
        }
    }
}
