﻿using FinancialManagerWin8.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace FinancialManagerWin8.Common.Converters
{
    public class CategoryTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string result = String.Empty;

            if (value != null)
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                string partLang = Enum.GetName(typeof(TransactionType), value);
                
                return loader.GetString("CategoryEditPage_" + partLang);

            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return String.Empty;
        }
    }
}
