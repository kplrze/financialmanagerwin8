﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManagerWin8.Common
{
    public static class AppHelpers
    {
        public static string[] GetMonths(bool shortForm)
        {
            try
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();

                string[] months = new string[12];

                months[0] = loader.GetString("Global_MonthJanuary");
                months[1] = loader.GetString("Global_MonthFebruary");
                months[2] = loader.GetString("Global_MonthMarch");
                months[3] = loader.GetString("Global_MonthApril");
                months[4] = loader.GetString("Global_MonthMay");
                months[5] = loader.GetString("Global_MonthJune");
                months[6] = loader.GetString("Global_MonthJuly");
                months[7] = loader.GetString("Global_MonthAugust");
                months[8] = loader.GetString("Global_MonthSeptember");
                months[9] = loader.GetString("Global_MonthOctober");
                months[10] = loader.GetString("Global_MonthNovember");
                months[11] = loader.GetString("Global_MonthDecember");

                if(shortForm == true)
                {
                    for(int i = 0; i < 12; i++)
                    {
                        months[i] = months[i].Substring(0, 3).ToUpper();
                    }
                }

                return months;
            }
            catch (Exception ex)
            {
                return new string[0];
            }
        }
    }
}
