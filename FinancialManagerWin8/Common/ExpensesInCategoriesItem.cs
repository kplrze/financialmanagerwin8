﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManagerWin8.Common
{
    public class ExpensesInCategoriesItem
    {
        public string CategoryName { get; set; }
        public object Amount { get; set; }
        public object PercentOfTotal { get; set; }
    }
}
