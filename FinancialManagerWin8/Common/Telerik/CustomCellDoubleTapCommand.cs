﻿using FinancialManagerWin8.DataAccessLayer;
using FinancialManagerWin8.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.UI.Xaml.Controls.Grid;
using Telerik.UI.Xaml.Controls.Grid.Commands;

namespace FinancialManagerWin8.Common.Telerik
{
    public class CustomCellTapCommand : DataGridCommand
    {
        public CustomCellTapCommand()
        {
            this.Id = CommandId.CellDoubleTap;
        }

        public IEntitiesViewModel ViewModel { get; set; }

        public override bool CanExecute(object parameter)
        {
            var context = parameter as DataGridCellInfo;
            return true;
        }

        public override void Execute(object parameter)
        {
            var context = parameter as DataGridCellInfo;

            IEntity entity = (IEntity)context.Item;

            (this.Owner.DataContext as IEntitiesViewModel).GoToEditEntityPage(entity.Id);
        }
    }
}
