﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinancialManagerWin8.Common.Telerik
{
    public class BarSeriesItem
    {
        public string Text { get; set; }
        public object Value { get; set; }
    }
}
