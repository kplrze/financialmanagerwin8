﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialManagerWin8.Model.Enums
{
    public enum TransactionType
    {
        Expense = 1,
        Income = 2
    }
}
