﻿using FinancialManagerWin8.DataAccessLayer;
using GalaSoft.MvvmLight;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialManagerWin8.Model
{
    [Table("Category")]
    public class Category : IEntity
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        [NotNull]
        public string Name { get; set; }
        [NotNull]
        public int Type { get; set; }
        [Ignore]
        public string TypeName { get; set; }
    }
}
