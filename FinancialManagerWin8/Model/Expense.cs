﻿using FinancialManagerWin8.DataAccessLayer;
using GalaSoft.MvvmLight;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinancialManagerWin8.Model
{
    [Table("Expense")]
    public class Expense : IEntity
    {
        [PrimaryKey, Unique, AutoIncrement]
        public int Id { get; set; }
        [NotNull]
        public DateTime DateCreated { get; set; }
        [NotNull]
        public string Name { get; set; }
        
        public int CategoryId { get; set; }
        public decimal Amount { get; set; }
        public string Details { get; set; }

        [Ignore]
        public string CategoryName { get; set; }
    }
}
